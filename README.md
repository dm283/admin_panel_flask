Общий функционал приложения: интерфейс администратора для работы с кадровой структурой организации - формирование справочников отделов/должностей/сотрудников/и пр., а так же их корректировка.
Реализован проект как веб-приложение (библиотека Flask, база данных Postgresql).
Использованные здесь данные по сотрудникам - фейковые.