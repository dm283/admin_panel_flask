from flask import Flask, render_template, url_for, request, redirect
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime, date, timedelta, time

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:s2d3f4!@@localhost/org_structure_2'

db = SQLAlchemy(app)

class OS_CHANGE(db.Model):
    __tablename__ = 'OS_CHANGE'
    ID=db.Column(db.Integer, primary_key=True)
    LOAD_DATE=db.Column(db.DateTime)
    ACTION_TYPE=db.Column(db.String(255))
    PARAM_1=db.Column(db.String(255))
    PARAM_1_TYPE=db.Column(db.String(255))
    PARAM_2=db.Column(db.String(255))
    PARAM_2_TYPE=db.Column(db.String(255))
    PARAM_3=db.Column(db.String(255))
    PARAM_3_TYPE=db.Column(db.String(255))
    PARAM_4=db.Column(db.String(255))
    PARAM_4_TYPE=db.Column(db.String(255))
    STATUS=db.Column(db.Integer)
    COMMIT_DATE=db.Column(db.DateTime)
    DESCRIPTION=db.Column(db.String(255))
    def __repr__(self):
        return '<ID %r>' % self.ID

class Iskra(db.Model):
    __tablename__ = 'USERS'
    ID=db.Column(db.Integer, primary_key=True)
    FIO=db.Column(db.String(250))
    USER_ID=db.Column(db.String(30))
    TAB_NUM=db.Column(db.Integer)
    def __repr__(self):
        return '<ID %r>' % self.ID

class Positions(db.Model):
    __tablename__ = 'POSITIONS'
    ID=db.Column(db.Integer, primary_key=True)
    POS_ID=db.Column(db.Integer)
    USER_ID=db.Column(db.String(30))
    POST_ID=db.Column(db.Integer)
    DEPART_ID=db.Column(db.Integer)
    def __repr__(self):
        return '<ID %r>' % self.ID

class Relations(db.Model):
    __tablename__ = 'RELATIONS'
    ID=db.Column(db.Integer, primary_key=True)
    POS_ID=db.Column(db.Integer)
    BOSS_POS_ID=db.Column(db.Integer)
    def __repr__(self):
        return '<ID %r>' % self.ID

class Posts(db.Model):
    __tablename__ = 'POSTS'
    ID=db.Column(db.Integer, primary_key=True)
    POST_ID=db.Column(db.Integer)
    POST_NAME=db.Column(db.String(250))
    def __repr__(self):
        return '<ID %r>' % self.ID

class Departs(db.Model):
    __tablename__ = 'DEPARTS'
    ID=db.Column(db.Integer, primary_key=True)
    DEPART_ID=db.Column(db.Integer)
    DEPART_NAME=db.Column(db.String(250))
    PARENT_ID=db.Column(db.Integer)
    def __repr__(self):
        return '<ID %r>' % self.ID

class VW_OS(db.Model):
    __tablename__ = 'VW_OS'
    FIO=db.Column(db.String(250))
    USER_ID=db.Column(db.String(30))
    TAB_NUM=db.Column(db.Integer)
    POS_ID=db.Column(db.Integer, primary_key=True)
    POST_ID=db.Column(db.Integer)
    POST_NAME=db.Column(db.String(250))
    DEPART_ID=db.Column(db.Integer)
    DEPART_NAME=db.Column(db.String(250))
    BOSS_POS_ID=db.Column(db.Integer)
    BOSS_FIO=db.Column(db.String(250))
    BOSS_TAB_NUM=db.Column(db.Integer)
    def __repr__(self):
        return '<POS_ID %r>' % self.POS_ID


@app.route('/', methods=['POST', 'GET'])
def index():
    if request.method == 'GET':
        rec_os_change = OS_CHANGE.query.order_by(OS_CHANGE.LOAD_DATE.desc()).limit(20).all()
        return render_template('index.html', rec_os_change=rec_os_change)

@app.route('/get_info', methods=['POST', 'GET'])
def get_info():
    if request.method == 'POST':
        try:
            tab_num = request.form['val_get_info']
            with open('tab_num_1.txt', 'w') as f1:
                f1.write(tab_num)
        except:
            with open('tab_num_1.txt', 'r') as f1:
                tab_num = int(f1.read())
        try:  
            tab_num_2 = request.form['val_get_info_2']
            with open('tab_num_2.txt', 'w') as f2:
                f2.write(tab_num_2)
        except:
            with open('tab_num_2.txt', 'r') as f2:
                tab_num_2 = int(f2.read())
    else:
        tab_num = 1559892
        with open('tab_num_1.txt', 'w') as f1:
            f1.write(str(tab_num))
        tab_num_2 = 1559892
        with open('tab_num_2.txt', 'w') as f2:
            f2.write(str(tab_num_2))  
    # запросы из БД для блока №1
    rec_iskra = Iskra.query.filter_by(TAB_NUM=tab_num).first()  # iskra
    rec_vw_os = VW_OS.query.filter_by(TAB_NUM=tab_num).first()  # vw_os
    # запросы из БД для блока №2 
    rec_iskra_2 = Iskra.query.filter_by(TAB_NUM=tab_num_2).first()  # iskra
    rec_vw_os_2 = VW_OS.query.filter_by(TAB_NUM=tab_num_2).first()  # vw_os

    return render_template('get_info.html', rec_iskra=rec_iskra, rec_vw_os=rec_vw_os,
        rec_iskra_2=rec_iskra_2, rec_vw_os_2=rec_vw_os_2)

# ========== TECH ====================
def put_msg(msg):
    with open("msg_tmp.txt", "w") as fl:
        fl.write(msg)

def get_msg():
    try:
         with open('msg_tmp.txt', 'r') as fl:
            msg = str(fl.read())
    except:
        msg = ""
    return msg
# =========== COMMANDS ===============
@app.route('/add_position', methods=['GET', 'POST'])
def add_position():
    # === POST METHOD ===
    if request.method == 'POST':
        # отмена команды -> перенаправление на главную страницу
        if request.form['submitted'] =='CANCEL / BACK':
            put_msg("")
            return redirect('/')
        # проверка POS_ID на корректный формат ввода
        if (request.form['val_pos_id'].isdigit() and
            request.form['val_post_id'].isdigit() and
            request.form['val_depart_id'].isdigit()):
            pos_id = int( request.form['val_pos_id'] )
            post_id = int( request.form['val_post_id'] )
            depart_id = int( request.form['val_depart_id'] )
        else:
            msg = "Incorrect input. POS_ID, POST_ID, DEPART_ID must be an integer."
            print(msg) ##
            put_msg(msg)
            return redirect('/add_position')
        # присваиваем user_id, если оно пустое то это вакансия
        if request.form['val_user_id'].strip():
            user_id = request.form['val_user_id'].strip()
        else:
            user_id = "<вакансия>"
        # проверка существования POS_ID, если есть - не добавляем
        row_in_db = Positions.query.filter_by(POS_ID=pos_id).first()
        if row_in_db:
            msg = "Unable to add this position, because it's already exists."
            print(msg) ##
            put_msg(msg)
            return redirect('/add_position') 
        # проверка существования POST_ID, если нет - не добавляем
        row_in_db = Posts.query.filter_by(POST_ID=post_id).first()
        if not row_in_db:
            msg = "Incorrent input: nonexistent POST_ID."
            put_msg(msg)
            return redirect('/add_position') 
        # проверка существования DEPART_ID, если нет - не добавляем
        row_in_db = Departs.query.filter_by(DEPART_ID=depart_id).first()
        if not row_in_db:
            msg = "Incorrent input: nonexistent DEPART_ID."
            put_msg(msg)
            return redirect('/add_position')
        # проверка USER_ID на сущестование (при заполненном поле) если нет - не добавляем
        if user_id!="<вакансия>":
            row_in_db = Iskra.query.filter_by(USER_ID=user_id).first()
            if not row_in_db:
                msg = "Incorrent input: nonexistent USER_ID."
                put_msg(msg)
                return redirect('/add_position')
            # доппроверка, что пользователя еще нет на позиции
            row_in_db = Positions.query.filter_by(USER_ID=user_id).first()
            if row_in_db:
                msg = "Unable to add this USER_ID on position, because it's already on position.."
                put_msg(msg)
                return redirect('/add_position')
        # если ввод корректный -> записываем даныне в бд
        new_rec = Positions(POS_ID=pos_id, USER_ID=user_id, POST_ID=post_id, DEPART_ID=depart_id)
        db.session.add(new_rec)
        db.session.commit()
        try:
            db.session.add(new_rec)
            db.session.commit()
            # логируем запись в бд
            new_rec = OS_CHANGE(ACTION_TYPE='ADD_POSITION', PARAM_1=pos_id, PARAM_1_TYPE='POS_ID',
                                PARAM_2=user_id, PARAM_2_TYPE='USER_ID',
                                PARAM_3=post_id, PARAM_3_TYPE='POST_ID',
                                PARAM_4=depart_id, PARAM_4_TYPE='DEPART_ID')
            db.session.add(new_rec)
            db.session.commit()
            put_msg(f"Successfully added position with id {pos_id}.")
            return redirect('/add_position')
        except:
            return 'There was an issue adding your record'
    # === GET METHOD ===
    else:
        msg = get_msg()
        return render_template('add_position.html', msg=msg)


@app.route('/remove_position', methods=['GET', 'POST'])
def remove_position():
    if request.method == 'POST':
        # отмена команды -> перенаправление на главную страницу
        if request.form['submitted'] =='CANCEL / BACK':
            put_msg("")
            return redirect('/')
        # проверка POS_ID на корректный формат ввода
        if request.form['val_pos_id'].isdigit() :
            pos_id = int( request.form['val_pos_id'] )
        else:
            msg = "Incorrect input. POS_ID must be an integer."
            print(msg) ##
            put_msg(msg)
            return redirect('/remove_position')
        # чтение из бд записи с введенным USER_ID
        row_to_delete = Positions.query.filter_by(POS_ID=pos_id).first()
        # проверка результата чтения из бд
        if row_to_delete:
            id_to_delete = row_to_delete.ID
        else:
            msg = f"There is no {pos_id} pos_id"
            print(msg) ##
            put_msg(msg)
            return redirect('/remove_position')
        # проверка что нет подчиненных позиций
        row_in_db = Relations.query.filter_by(BOSS_POS_ID=pos_id).first()
        if row_in_db:
            msg = f"Unable to delete this position, because it has subordinates."
            print(msg) ##
            put_msg(msg)    
            return redirect('/remove_position')
        # если все верно пишем в бд
        id_to_delete = Positions.query.get_or_404( id_to_delete )
        try:
            db.session.delete(id_to_delete)
            db.session.commit()
            # логируем запись в бд
            new_rec = OS_CHANGE(ACTION_TYPE='REMOVE_POSITION', PARAM_1=pos_id, PARAM_1_TYPE='POS_ID')
            db.session.add(new_rec)
            db.session.commit()
            msg = f"Recording with POS_ID={pos_id} is successfully deleted."
            print(msg) ##
            put_msg(msg)
            return redirect('/remove_position')
        except:
            return 'There was a problem deleting a post!'
    else:
        msg = get_msg()
        return render_template('remove_position.html', msg=msg)


@app.route('/add_user', methods=['GET', 'POST'])
def add_user():
    # === POST METHOD ===
    if request.method == 'POST':
        # отмена команды -> перенаправление на главную страницу
        if request.form['submitted'] =='CANCEL / BACK':
            put_msg("")
            return redirect('/')
        # проверка TAB_NUM на корректный формат ввода
        if request.form['val_tab_num'].isdigit():
            tab_num = int( request.form['val_tab_num'] )
        else:
            msg = "Incorrect input. TAB_NUM must be an integer."
            print(msg) ##
            put_msg(msg)
            return redirect('/add_user')
        # присваиваем user_id и имя без проверки
        user_id = request.form['val_user_id']
        fio = request.form['val_fio']
        # проверка существования USER_ID, если есть - не добавляем
        row_in_db = Iskra.query.filter_by(USER_ID=user_id).first()
        if row_in_db:
            msg = "Unable to add this USER_ID, because it's already exists."
            print(msg) ##
            put_msg(msg)
            return redirect('/add_user')   
        # проверка существования TAB_NUM, если есть - не добавляем
        row_in_db = Iskra.query.filter_by(TAB_NUM=tab_num).first()
        if row_in_db:
            msg = "Unable to add this TAB_NUM, because it's already exists."
            print(msg) ##
            put_msg(msg)
            return redirect('/add_user')  

        # если ввод корректный -> записываем даныне в бд
        new_rec = Iskra(FIO=fio, USER_ID=user_id, TAB_NUM=tab_num)
        print("**************", type(fio), type(user_id), type(tab_num), new_rec)
        db.session.add(new_rec)
        db.session.commit()
        print("YES!!!!!!!!!!")
        try:
            db.session.add(new_rec)
            db.session.commit()
            print("=========OK==========")
            # логируем запись в бд
            new_rec = OS_CHANGE(ACTION_TYPE='ADD_USER', PARAM_1=user_id, PARAM_1_TYPE='USER_ID',
                                PARAM_2=fio, PARAM_2_TYPE='FIO',
                                PARAM_3=tab_num, PARAM_3_TYPE='TAB_NUM')
            db.session.add(new_rec)
            db.session.commit()
            put_msg(f"Successfully added user with id {user_id}.")
            return redirect('/add_user')
        except:
            return 'There was an issue adding your record'
    # === GET METHOD ===
    else:
        msg = get_msg()
        return render_template('add_user.html', msg=msg)


@app.route('/remove_user', methods=['GET', 'POST'])
def remove_user():
    if request.method == 'POST':
        # отмена команды -> перенаправление на главную страницу
        if request.form['submitted'] =='CANCEL / BACK':
            put_msg("")
            return redirect('/')
        # проверка поле не пустое
        if str(request.form['val_user_id']).strip():
            user_id = request.form['val_user_id']
        else:
            msg = "Input USER_ID."
            print(msg) ##
            put_msg(msg)
            return redirect('/remove_user')        
        # чтение из бд записи с введенным USER_ID
        row_to_delete = Iskra.query.filter_by(USER_ID=user_id).first()
        # проверка результата чтения из бд
        if row_to_delete:
            id_to_delete = row_to_delete.ID
        else:
            msg = f"There is no {user_id} user_id"
            print(msg) ##
            put_msg(msg)
            return redirect('/remove_user')
        # проверка, что нет позиций с удаляемым пользователем
        row_in_positions = Positions.query.filter_by(USER_ID=user_id).first()
        if row_in_positions:
            msg = f"Unable to delete this user, because there is the position with it."
            print(msg) ##
            put_msg(msg)    
            return redirect('/remove_user')
        # если все верно пишем в бд
        id_to_delete = Iskra.query.get_or_404( id_to_delete )
        print(id_to_delete)  ##########
        try:
            db.session.delete(id_to_delete)
            db.session.commit()
            # логируем запись в бд
            new_rec = OS_CHANGE(ACTION_TYPE='REMOVE_USER', PARAM_1=user_id, PARAM_1_TYPE='USER_ID')
            db.session.add(new_rec)
            db.session.commit()
            msg = f"Recording with USER_ID={user_id} is successfully deleted."
            print(msg) ##
            put_msg(msg)
            return redirect('/remove_user')
        except:
            return 'There was a problem deleting a post!'
    else:
        msg = get_msg()
        return render_template('remove_user.html', msg=msg)


@app.route('/add_depart', methods=['GET', 'POST'])
def add_depart():
    # === POST METHOD ===
    if request.method == 'POST':
        # отмена команды -> перенаправление на главную страницу
        if request.form['submitted'] =='CANCEL / BACK':
            put_msg("")
            return redirect('/')
        # проверка DEPART_ID на корректный формат ввода
        if request.form['val_depart_id'].isdigit() :
            depart_id = int( request.form['val_depart_id'] )
        else:
            msg = "Incorrect input. DEPART_ID and PARENT_ID must be an integer."
            print(msg) ##
            put_msg(msg)
            return redirect('/add_depart')
        # проверка PARENT_ID на корректный формат ввода (при наличии)
        if request.form['val_parent_id']:
            if request.form['val_parent_id'].isdigit():
                parent_id = int( request.form['val_parent_id'] )
            else:
                msg = "Incorrect input. DEPART_ID and PARENT_ID must be an integer."
                print(msg) ##
                put_msg(msg)
                return redirect('/add_depart')
        else:
            parent_id = None
        # присваиваем имя без проверки
        depart_name = request.form['val_depart_name']
        # проверка существования DEPART_ID, если есть - не добавляем
        row_in_db = Departs.query.filter_by(DEPART_ID=depart_id).first()
        if row_in_db:
            msg = "Unable to add this depart, because it's already exists."
            print(msg) ##
            put_msg(msg)
            return redirect('/add_depart')   
        # проверка существования PARENT_ID, если нет - не добавляем
        if request.form['val_parent_id']:
            row_in_db = Departs.query.filter_by(DEPART_ID=parent_id).first()
            if not row_in_db:
                msg = "Incorrent input: nonexistent PARENT_ID."
                put_msg(msg)
                return redirect('/add_depart')  
        # если ввод корректный -> записываем даныне в бд
        new_rec = Departs(DEPART_ID=depart_id, DEPART_NAME=depart_name, PARENT_ID=parent_id)
        try:
            db.session.add(new_rec)
            db.session.commit()
            # логируем запись в бд
            new_rec = OS_CHANGE(ACTION_TYPE='ADD_DEPART', PARAM_1=depart_id, PARAM_1_TYPE='DEPART_ID',
                                PARAM_2=depart_name, PARAM_2_TYPE='DEPART_NAME',
                                PARAM_3=parent_id, PARAM_3_TYPE='PARENT_ID')
            db.session.add(new_rec)
            db.session.commit()
            put_msg(f"Successfully added depart with id {depart_id}.")
            return redirect('/add_depart')
        except:
            return 'There was an issue adding your record'
    # === GET METHOD ===
    else:
        msg = get_msg()
        return render_template('add_depart.html', msg=msg)


@app.route('/remove_depart', methods=['GET', 'POST'])   # direct var
def remove_depart():
    if request.method == 'POST':
        # отмена команды -> перенаправление на главную страницу
        if request.form['submitted'] =='CANCEL / BACK':
            put_msg("")
            return redirect('/')
        # проверка на корректный формат ввода
        if request.form['val_depart_id'].isdigit():
            depart_id = int( request.form['val_depart_id'] )
        else:
            msg = "Incorrect input. DEPART_ID must be an integer."
            print(msg) ##
            put_msg(msg)
            return redirect('/remove_depart')
        # чтение из бд записи с введенным DEPART_ID
        row_to_delete = Departs.query.filter_by(DEPART_ID=depart_id).first()
        # проверка результата чтения из бд
        if row_to_delete:
            id_to_delete = row_to_delete.ID
        else:
            msg = f"There is no {depart_id} depart_id"
            print(msg) ##
            put_msg(msg)
            return redirect('/remove_depart')
        # проверка, что нет сотрудников с удаляемым подразделеним
        row_in_positions = Positions.query.filter_by(DEPART_ID=depart_id).first()
        if row_in_positions:
            msg = f"Unable to delete this depart, because there is the position with it."
            print(msg) ##
            put_msg(msg)    
            return redirect('/remove_depart')

        id_to_delete = Departs.query.get_or_404( id_to_delete )
        print(id_to_delete)  ##########
        try:
            db.session.delete(id_to_delete)
            db.session.commit()
            # логируем запись в бд
            new_rec = OS_CHANGE(ACTION_TYPE='REMOVE_DEPART', PARAM_1=depart_id, PARAM_1_TYPE='DEPART_ID')
            db.session.add(new_rec)
            db.session.commit()
            msg = f"Recording with DEPART_ID={depart_id} is successfully deleted."
            print(msg) ##
            put_msg(msg)
            return redirect('/remove_depart')
        except:
            return 'There was a problem deleting a post!'
    else:
        msg = get_msg()
        return render_template('remove_depart.html', msg=msg)


@app.route('/add_post', methods=['GET', 'POST'])
def add_post():
    if request.method == 'POST':
        # отмена команды -> перенаправление на главную страницу
        if request.form['submitted'] =='CANCEL / BACK':
            put_msg("")
            return redirect('/')

        # проверка на корректный формат ввода
        if request.form['val_post_id'].isdigit():
            post_id = int( request.form['val_post_id'] )
        else:
            msg = "Incorrect input. POST_ID must be an integer."
            print(msg) ##
            put_msg(msg)
            return redirect('/add_post')

        post_name = request.form['val_post_name']
        # проверка существования POST_ID, если есть - не добалвяем
        row_in_posts = Posts.query.filter_by(POST_ID=post_id).first()
        if row_in_posts:
            msg = "Unable to add this post, because it's already exists."
            print(msg) ##
            put_msg(msg)
            return redirect('/add_post')

        new_rec = Posts(POST_ID=post_id, POST_NAME=post_name)
        print("new_rec =", new_rec)              ##
        try:
            db.session.add(new_rec)
            db.session.commit()
            # логируем запись в бд
            new_rec = OS_CHANGE(ACTION_TYPE='ADD_POST', PARAM_1=post_id, PARAM_1_TYPE='POST_ID',
                                    PARAM_2=post_name, PARAM_2_TYPE='POST_NAME')
            db.session.add(new_rec)
            db.session.commit()
            put_msg(f"Successfully added post with id {post_id}.")
            return redirect('/add_post')
        except:
            return 'There was an issue adding your record'
    else:  # GET METHOD
        msg = get_msg()
        return render_template('add_post.html', msg=msg)


@app.route('/remove_post', methods=['GET', 'POST'])   # direct var
def remove_post():
    if request.method == 'POST':
        # отмена команды -> перенаправление на главную страницу
        if request.form['submitted'] =='CANCEL / BACK':
            return redirect('/')
        # проверка на корректный формат ввода
        if request.form['val_post_id'].isdigit():
            post_id = int( request.form['val_post_id'] )
            print(post_id) ##########
        else:
            print("Incorrect input. POST_ID must be an integer.") ## добавить вывод сообщения в форму!
            return redirect('/remove_post')
        # чтение из бд записи с введенным POST_ID
        row_to_delete = Posts.query.filter_by(POST_ID=post_id).first()
        # проверка результата чтения из бд
        if row_to_delete:
            id_to_delete = row_to_delete.ID
        else:
            print(f"There is no {post_id} post_id")   ## добавить вывод сообщения в форму!
            return redirect('/remove_post')
        # проверка, что нет сотрудников с удаляемой должностью
        row_in_positions = Positions.query.filter_by(POST_ID=post_id).first()
        if row_in_positions:
            print(f"Unable to delete this post, because there is the position with it.")
            return redirect('/remove_post')

        id_to_delete = Posts.query.get_or_404( id_to_delete )
        print(id_to_delete)  ##########
        try:
            db.session.delete(id_to_delete)
            db.session.commit()
            # логируем запись в бд
            new_rec = OS_CHANGE(ACTION_TYPE='REMOVE_POST', PARAM_1=post_id, PARAM_1_TYPE='POST_ID')
            db.session.add(new_rec)
            db.session.commit()
            print(f"Recording with POST_ID={post_id} is successfully deleted.") ## добавить вывод сообщения в форму!
            return redirect('/')
        except:
            return 'There was a problem deleting a post!'
    else:
        msg = get_msg()
        return render_template('remove_post.html', msg=msg)




# @app.route('/set_relation', methods=['GET', 'POST'])
# def set_relation():
#     if request.method == 'POST':
#         pos_id = request.form['val_pos_id']
#         boss_pos_id = request.form['val_boss_pos_id']

#         new_rec = OS_CHANGE(ACTION_TYPE='SET_RELATION', PARAM_1=pos_id, PARAM_1_TYPE='POS_ID', 
#                                     PARAM_2=boss_pos_id, PARAM_2_TYPE='POS_ID')
                                    
#         try:
#             db.session.add(new_rec)
#             db.session.commit()
#             return redirect('/')
#         except:
#             return 'There was an issue adding your record'
#     else:
#         return render_template('set_relation.html')

# @app.route('/remove_relation', methods=['GET', 'POST'])
# def remove_relation():
#     if request.method == 'POST':
#         pos_id = request.form['val_pos_id']
#         boss_pos_id = request.form['val_boss_pos_id']

#         new_rec = OS_CHANGE(ACTION_TYPE='REMOVE_RELATION', PARAM_1=pos_id, PARAM_1_TYPE='POS_ID', 
#                                     PARAM_2=boss_pos_id, PARAM_2_TYPE='POS_ID')
                                    
#         try:
#             db.session.add(new_rec)
#             db.session.commit()
#             return redirect('/')
#         except:
#             return 'There was an issue adding your record'
#     else:
#         return render_template('remove_relation.html')

# @app.route('/change_depart', methods=['GET', 'POST'])
# def change_depart():
#     if request.method == 'POST':
#         pos_id = request.form['val_pos_id']
#         depart_id = request.form['val_depart_id']

#         new_rec = OS_CHANGE(ACTION_TYPE='CHANGE_DEPART', PARAM_1=pos_id, PARAM_1_TYPE='POS_ID', 
#                                     PARAM_2=depart_id, PARAM_2_TYPE='DEPART_ID')

#         try:
#             db.session.add(new_rec)
#             db.session.commit()
#             return redirect('/')
#         except:
#             return 'There was an issue adding your record'
#     else:
#         return render_template('change_depart.html')

# @app.route('/change_post', methods=['GET', 'POST'])
# def change_post():
#     if request.method == 'POST':
#         pos_id = request.form['val_pos_id']
#         post_id = request.form['val_post_id']

#         new_rec = OS_CHANGE(ACTION_TYPE='CHANGE_POST', PARAM_1=pos_id, PARAM_1_TYPE='POS_ID', 
#                                     PARAM_2=post_id, PARAM_2_TYPE='POST_ID')

#         try:
#             db.session.add(new_rec)
#             db.session.commit()
#             return redirect('/')
#         except:
#             return 'There was an issue adding your record'
#     else:
#         return render_template('change_post.html')

# @app.route('/change_fio', methods=['GET', 'POST'])
# def change_fio():
#     if request.method == 'POST':
#         user_id = request.form['val_user_id']
#         fio = request.form['val_fio']

#         new_rec = OS_CHANGE(ACTION_TYPE='CHANGE_FIO', PARAM_1=user_id, PARAM_1_TYPE='USER_ID', 
#                                     PARAM_2=fio, PARAM_2_TYPE='FIO')

#         try:
#             db.session.add(new_rec)
#             db.session.commit()
#             return redirect('/')
#         except:
#             return 'There was an issue adding your record'
#     else:
#         return render_template('change_fio.html')



#@app.route('/delete/<int:id_rec>')
#def delete(id_rec):
#    position_to_delete = Structure.query.get_or_404(id_rec)
#    print(position_to_delete)
#
#    try:
#        db.session.delete(position_to_delete)
#        db.session.commit()
#        return redirect('/')
#    except:
#        return 'There was a problem deleting a task'


# @app.route('/change_depart_name', methods=['GET', 'POST'])
# def change_depart_name():
#     if request.method == 'POST':
#         depart_id = request.form['val_depart_id']
#         depart_name_new = request.form['val_depart_name_new']

#         new_rec = OS_CHANGE(ACTION_TYPE='CHANGE_DEPART_NAME', 
#                                     PARAM_1=depart_id, PARAM_1_TYPE='DEPART_ID', 
#                                     PARAM_2=depart_name_new, PARAM_2_TYPE='EMPTY')

#         try:
#             db.session.add(new_rec)
#             db.session.commit()
#             return redirect('/')
#         except:
#             return 'There was an issue adding your record'
#     else:
#         return render_template('change_depart_name.html')

# @app.route('/add_user_on_post', methods=['GET', 'POST'])
# def add_user_on_post():
#     if request.method == 'POST':
#         pos_id = request.form['val_pos_id']
#         user_id = request.form['val_user_id']

#         new_rec = OS_CHANGE(ACTION_TYPE='ADD_USER_ON_POST', PARAM_1=pos_id, PARAM_1_TYPE='POS_ID', 
#                                     PARAM_2=iskra_user_id, PARAM_2_TYPE='ISKRA_USER_ID')

#         try:
#             db.session.add(new_rec)
#             db.session.commit()
#             return redirect('/')
#         except:
#             return 'There was an issue adding your record'
#     else:
#         return render_template('add_user_on_post.html')

# @app.route('/remove_user_from_post', methods=['GET', 'POST'])
# def remove_user_from_post():
#     if request.method == 'POST':
#         pos_id = request.form['val_pos_id']

#         new_rec = OS_CHANGE(ACTION_TYPE='REMOVE_USER_FROM_POST', PARAM_1=pos_id, PARAM_1_TYPE='POS_ID')

#         try:
#             db.session.add(new_rec)
#             db.session.commit()
#             return redirect('/')
#         except:
#             return 'There was an issue adding your record'
#     else:
#         return render_template('remove_user_from_post.html')


# @app.route('/update/<int:id>', methods=['GET', 'POST'])
# def update(id):
#     task = Todo.query.get_or_404(id)

#     if request.method == 'POST':
#         task.content = request.form['content']

#         try:
#             db.session.commit()
#             return redirect('/')
#         except:
#             return 'There was an issue updating your task'
#     else:
#         return render_template('update.html', task=task)

if __name__ == '__main__':
    app.run(debug=True)
